@auth
{!! Form::open(['method'=>'DELETE','route' =>['productos.destroy',$products->id], 'onsubmit'=>'return confirm("¿Estas seguro que deseas eliminar este producto ?")']) !!}
	<input type="submit" value="Eliminar producto" class="btn btn-danger">
{!! Form::close() !!}
@endauth