@extends("layouts.app")

@section('content')
<div class="container">
	<header>
		<div class="card">
			<h4>Crear un nuevo producto</h4>
		</div>
	</header>
	<div class="card-body">
		@include('products.form')
	</div>
</div>
@endsection