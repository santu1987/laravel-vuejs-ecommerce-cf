{!! Form::open(['route' =>[$products->url(),$products->id],'method'=>$products->method(),'class'=>'app-form']) !!}
	<div>
		{!! Form::label('title','Título del producto') !!}
		{!! Form::text('title',$products->title,['class'=>'form-control']) !!}
	</div>
	
	<div>
		{!! Form::label('description','Descripción del producto') !!}
		{!! Form::textarea('description',$products->description,['class'=>'form-control']) !!}
	</div>


	<div>
		{!! Form::label('price','Precio del producto') !!}
		{!! Form::number('price',$products->price,['class'=>'form-control']) !!}
	</div>

	<div>
		<input type="submit" name="guardar" class="btn btn-primary">
	</div>
{!! Form::close() !!}