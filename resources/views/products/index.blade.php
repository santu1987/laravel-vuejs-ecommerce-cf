@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			@foreach($products as $product)
				<div class="col-xs-12 col-sm-8 col-md-4">
					<div class="card padding">
						<header>
							<a href="/productos/{{$product->id}}">
								<h2 class="card-title">{{$product->title}}</h2>
							</a>
							<h2 class="card-subtitle">{{$product->price}}</h2>
						</header>
						<p class="card-text">
							{{$product->description}}
						</p>
					</div>
				</div>
			@endforeach
		</div>
		<!-- Para la paginacion -->
		<div class="actions ">
			{{$products->links()}}
		</div>
	</div>
@endsection