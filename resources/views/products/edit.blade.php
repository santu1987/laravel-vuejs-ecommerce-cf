@extends("layouts.app")

@section('content')
<div class="container">
	<header>
		<div class="card">
			<h4>Edita un producto</h4>
			<p>{{$products->title}}</p>
		</div>
	</header>
	<div class="card-body">
		@include('products.form')
	</div>
</div>
@endsection