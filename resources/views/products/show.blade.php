@extends('layouts.app')

@section('content')
	<div class="row justify-content-sm-center">
		<div class="col-xs-qw col-sm-10 col-md-7 col-lg-6">
			<div class="card">
				<header class="padding text-center bg-primary">
					
				</header>
				<div class="card-body padding">
					<h1 class="card-title">{{$products->title}}</h1>
					<h4 class="card-subtitle">{{$products->price}}</p>
					<p class="card-text">{{$products->description}}</p>

					<div class="card-actions">
						<button type="button" name="button" class="btn btn-success">Agregar al carrito</button>
						@include('products/delete')
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
