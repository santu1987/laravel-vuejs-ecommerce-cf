<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;

class ValidateFirstUserSignUp
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userCount = User::count();
        //Si hay mas de un usuario en la bd y no se inicio session
        if($userCount>0 && Auth::check()) {
            return redirect("/");
        }       
            return $next($request);
    }
}
